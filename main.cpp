#include <iostream>
#include <string>
#include <cmath>
#include <unistd.h>

using namespace std;

string cus_sys(int a, int jaki_system)
{
    int _system = jaki_system;
    string tab = "0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*([{)]};:<>?~";
    
    if(_system > tab.length())
        _system = tab.length();
    
    if(_system <= 1)
        return "";
    
    if(a <= 0)
        return "0";
    
    string result = "";
    int miejsc = 0;
    int tmp = a;
    while(tmp > 0)
    {
        miejsc++;
        tmp = tmp/_system;
    }
    
    int ind = 0;
    tmp = a;
    
    for(int i = miejsc; i > 0; i--)
    {
        ind = tmp / pow(_system,i-1);
        tmp = tmp % static_cast<int>(pow(_system,i-1));
        result += tab[ind];
    }
    
    return result;
}



int When_then(int liczba, string ocz_wynik, int _system)
{
    if(cus_sys(liczba,_system) == ocz_wynik)
    {
        cout << _system << "_When " << liczba << " then " << ocz_wynik << " accomplished: " << cus_sys(liczba,_system) << endl;
        return 0;
    }
    else
    {
        cout << _system << "_When " << liczba << " then " << ocz_wynik << " failed: " << cus_sys(liczba,_system) << endl;
        return 1;
    }
}


int main()
{
    
    string result_i = "";
    int _system = 3;
    int max_len = 5;
    cout << "System: "; cin >> _system;
    cout << "Maks dlugosc: "; cin >> max_len;
    
    
    for(int i = 0; ; i++)
    {
        result_i = cus_sys(i, _system);
        if(result_i.length() == max_len + 1)
        {
            break;
        }
        cout << result_i << endl;
    }
	return 0;
}


